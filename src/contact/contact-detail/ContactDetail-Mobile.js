import classes from "./ContactDetail-Mobile.module.css";

const ContactDetailMobile = (props) => {
  const user = props.user;
  const company = user.company;
  const address = user.address;
  const avatarUrl = props.avatarUrl;

  return (
    <div className={classes.contact}>
      <h2 className={classes.title}>Contact Detail</h2>
      <div className={classes.personal}>
        <img className={classes.avatar} src={avatarUrl} alt="Avatar"></img>

        <div className={classes["personal-detail"]}>
          <div className={classes["detail-item"]}>
            <label>Name :</label>
            <span>{user.name}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>Email :</label>
            <span>{user.email}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>Username :</label>
            <span>{user.username}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>Phone :</label>
            <span>{user.phone}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>Website :</label>
            <span>{user.website}</span>
          </div>
        </div>
      </div>
      <h2 className={classes.title}>Company</h2>
      <div className={classes.company}>
        <div className={classes["company-detail"]}>
          <div className={classes["detail-item"]}>
            <label>Name :</label>
            <span>{company.name}</span>
          </div>

          <div className={classes["detail-item"]}>
            <label>BS :</label>
            <span>{company.bs}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>CatchPhrase :</label>
            <span>{company.catchPhrase}</span>
          </div>
        </div>
      </div>
      <h2 className={classes.title}>Address</h2>
      <div className={classes.address}>
        <div className={classes["address-detail"]}>
          <div className={classes["detail-item"]}>
            <label>Suite :</label>
            <span>{address.suite}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>Street :</label>
            <span>{address.street}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>City :</label>
            <span>{address.city}</span>
          </div>
          <div className={classes["detail-item"]}>
            <label>ZipCode :</label>
            <span>{address.zipcode}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactDetailMobile;
