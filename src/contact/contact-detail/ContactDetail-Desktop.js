import classes from "./ContactDetail-Desktop.module.css";
import React from "react";

// import GoogleMapReact from "google-map-react";

// const AnyReactComponent = ({ text }) => <div>{text}</div>;
// const defaultProps = {
//   center: {
//     lat: 10.99835602,
//     lng: 77.01502627,
//   },
//   zoom: 11,
// };
const ContactDetailDesktop = (props) => {
  const user = props.user;
  const company = user.company;
  const address = user.address;
  const avatarUrl = props.avatarUrl;

  return (
    <>
      <div className={classes.contact}>
        <h2 className={classes.title}>Contact Detail</h2>
        <div className={classes.personal}>
          <img className={classes.avatar} src={avatarUrl} alt="Avatar"></img>

          <div className={classes["personal-detail"]}>
            <div
              className={`${classes["detail-item"]} ${classes["personal-detail-item"]}`}
            >
              <label>Name :</label>
              <span>{user.name}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["personal-detail-item"]}`}
            >
              <label>Email :</label>
              <span>{user.email}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["personal-detail-item"]}`}
            >
              <label>Username :</label>
              <span>{user.username}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["personal-detail-item"]}`}
            >
              <label>Phone :</label>
              <span>{user.phone}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["personal-detail-item"]}`}
            >
              <label>Website :</label>
              <span>{user.website}</span>
            </div>
          </div>
        </div>
        <h2 className={classes.title}>Company</h2>
        <div className={classes.company}>
          <div className={classes["company-detail"]}>
            <div
              className={`${classes["detail-item"]} ${classes["company-detail-item"]}`}
            >
              <label>Name :</label>
              <span>{company.name}</span>
            </div>

            <div
              className={`${classes["detail-item"]} ${classes["company-detail-item"]}`}
            >
              <label>BS :</label>
              <span>{company.bs}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["company-detail-item"]}`}
            >
              <label>CatchPhrase :</label>
              <span>{company.catchPhrase}</span>
            </div>
          </div>
        </div>
        <h2 className={classes.title}>Address</h2>
        <div className={classes.address}>
          <div className={classes["address-detail"]}>
            <div
              className={`${classes["detail-item"]} ${classes["address-detail-item"]}`}
            >
              <label>Suite :</label>
              <span>{address.suite}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["address-detail-item"]}`}
            >
              <label>Street :</label>
              <span>{address.street}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["address-detail-item"]}`}
            >
              <label>City :</label>
              <span>{address.city}</span>
            </div>
            <div
              className={`${classes["detail-item"]} ${classes["address-detail-item"]}`}
            >
              <label>ZipCode :</label>
              <span>{address.zipcode}</span>
            </div>
          </div>
          {/* <div className={classes["map-container"]}>
            <div style={{ height: "100%" }}>
              <GoogleMapReact
                bootstrapURLKeys={{
                  key: "AIzaSyB1iIcpKlnuQNCxr9CDzbxU4Kurs1nQQJI",
                }}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
              >
                <AnyReactComponent
                  lat={59.955413}
                  lng={30.337844}
                  text="My Marker"
                />
              </GoogleMapReact>
            </div>
          </div> */}
        </div>
      </div>
    </>
  );
};

export default ContactDetailDesktop;
