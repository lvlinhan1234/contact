import classes from "./ContactItem.module.css";

import { BsFillTelephoneFill } from "react-icons/bs";
import { MdEmail } from "react-icons/md";

const ContactItem = (props) => {
  const user = props.user;
  const index = props.index;
  const avatarUrl = props.avatarUrl;

  const showSlideHandler = () => {
    props.onSlideShow(index);
  };

  return (
    <div className={classes.user} onClick={showSlideHandler}>
      <img src={avatarUrl} alt="Avatar"></img>
      <div className={classes["user-info"]}>
        <div className={classes.name}>{user.name}</div>
        <div className={classes["user-contact"]}>
          <BsFillTelephoneFill size={16} color="lightgreen" />
          {user.phone}
        </div>
        <div className={classes["user-contact"]}>
          <MdEmail size={16} color="yellow" />
          {user.email}
        </div>
      </div>
    </div>
  );
};

export default ContactItem;
