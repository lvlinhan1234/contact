import { Fragment } from "react";
import { useMediaQuery } from "react-responsive";
import ContactDetailDesktop from "./contact-detail/ContactDetail-Desktop";
import ContactDetailMobile from "./contact-detail/ContactDetail-Mobile";

const ContactDetail = (props) => {
  const smallDevice = useMediaQuery({
    query: "(max-width: 599px)",
  });
  return (
    <Fragment>
      {!smallDevice && (
        <ContactDetailDesktop user={props.user} avatarUrl={props.avatarUrl} />
      )}
      {smallDevice && (
        <ContactDetailMobile user={props.user} avatarUrl={props.avatarUrl} />
      )}
    </Fragment>
  );
};

export default ContactDetail;
