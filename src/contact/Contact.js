import ContactList from "./ContactList";

const Contact = (props) => {
  return <ContactList users={props.users} avatars={props.avatars} />;
};

export default Contact;
