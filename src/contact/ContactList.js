import classes from "./ContactList.module.css";
import ContactItem from "./ContactItem";
import userIcon from "../../src/user-thumb_128.png";
import { useState } from "react";
import ContactDetail from "./ContactDetail";
import Slider from "../slide/Slider";
import ReactDOM from "react-dom";

const ContactList = (props) => {
  const users = props.users;
  const avatars = props.avatars;

  const [showSlide, setShowSlide] = useState(false);
  const [showUserIndex, setShowUserIndex] = useState();

  const showSlideHandler = (index) => {
    setShowUserIndex(index);
    setShowSlide(true);
  };

  const hideSlideHandler = () => {
    setShowSlide(false);
  };

  const slidePortal = document.getElementById("slide");

  return (
    <>
      <div className={`${classes["contact-list-container"]}`}>
        {
          <div className={classes["contact-list"]}>
            {users.map((user, index) => (
              <div key={user.id} className={classes["user-container"]}>
                <ContactItem
                  user={user}
                  avatarUrl={
                    avatars.length > 0 ? avatars[index].medium : userIcon
                  }
                  index={index}
                  onSlideShow={showSlideHandler}
                />
              </div>
            ))}
          </div>
        }
      </div>

      {showSlide &&
        ReactDOM.createPortal(
          <Slider onHideSlide={hideSlideHandler}>
            <ContactDetail
              user={users[showUserIndex]}
              avatarUrl={
                avatars.length > 0 ? avatars[showUserIndex].large : userIcon
              }
            />
          </Slider>,
          slidePortal
        )}
    </>
  );
};

export default ContactList;
