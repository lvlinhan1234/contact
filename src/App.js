import "./App.css";
import { useEffect } from "react";
import { useState } from "react";
import Contact from "./contact/Contact";

let avatarFetched = false;

const fetchUsers = async (setUsers) => {
  const response = await fetch("https://jsonplaceholder.typicode.com/users", {
    method: "GET",
  });
  const data = await response.json();
  setUsers(data);
};

const fetchAvatas = async (setAvatars) => {
  if (avatarFetched === true) return;
  avatarFetched = true;

  const response = await fetch("https://randomuser.me/api/?results=10", {
    method: "GET",
  });
  const data = await response.json();
  const avatars = data.results.map((result) => {
    const picture = result.picture;
    return {
      medium: picture.medium,
      large: picture.large,
      small: picture.thumbnail,
    };
  });
  setAvatars(avatars);
};

function App() {
  const [users, setUsers] = useState([]);
  const [avatars, setAvatars] = useState([]);

  useEffect(() => {
    fetchUsers(setUsers);
    fetchAvatas(setAvatars);
  }, []);

  return (
    <div className="App">
      <div className="app-content">
        <Contact users={users} avatars={avatars} />
      </div>
      <div id="slide"></div>
    </div>
  );
}

export default App;
