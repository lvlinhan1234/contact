import classes from "./Slider.module.css";
import { useMediaQuery } from "react-responsive";
import { IoArrowBackCircle } from "react-icons/io5";

const Slider = (props) => {
  const smallDevice = useMediaQuery({
    query: "(max-width: 599px)",
  });
  return (
    <>
      {smallDevice && (
        <>
          <div className={classes.backdrop}></div>
          <div className={`${classes.content} ${classes["content-mobile"]}`}>
            {props.children}
          </div>
          <div onClick={props.onHideSlide}>
            <IoArrowBackCircle
              className={classes["back-icon-mobile"]}
              size={50}
              color={"gray"}
            />
          </div>
        </>
      )}
      {!smallDevice && (
        <>
          <div className={classes.backdrop} onClick={props.onHideSlide}></div>
          <div className={`${classes.content} ${classes["content-desktop"]}`}>
            {props.children}
            <div className={classes["actions-desktop"]}>
              <button
                className={classes["close-btn-desktop"]}
                onClick={props.onHideSlide}
              >
                Close
              </button>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Slider;
